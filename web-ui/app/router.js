import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('platform-config');
  this.route('member-mgt');
  this.route('user-mgt');
  this.route('logs');
  this.route('coms-fees');
});

export default Router;
