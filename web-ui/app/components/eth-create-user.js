// Import build-time config
import config from '../config/environment';
import Ember from 'ember';
import UiModal from 'semantic-ui-ember/components/ui-modal';

const {APP: {apiBaseUrl: API_BASE_URL }} = config;
const ACTION_ON_SUCCESS = 'onSuccess';

export default UiModal.extend({
  classNames: ['eth-create-user'],
  actions: {
    yes: function() {
      // TODO Execute Cloud Function here
      const data = this.get('model');
      if (!this.validate(data)) {
        return;
      }
      // TODO Refactor to a service
      Ember.$.ajax({
        method: 'POST',
        url: `${API_BASE_URL}users--createUser`,
        data,
        dateType: 'json'
      }).promise().then(() => {
        this.set('model', {
          username: '',
          email: '',
          roles: [],
          status: 'active'
        });
        this.set('validation', {errors:{}, serverError: null});
        this.sendAction(ACTION_ON_SUCCESS);
        this.execute('hide');
      }).catch(e => {
        this.set('validation.serverError', e.responseText || 'Unknown');
      });
    },

    no: function() {
      this.execute('hide');
    }
  },
  roles: [
    'administrator',
    'moderator',
    'editor'
  ],
  model: {
    username: '',
    email: '',
    roles: [],
    status: 'active'
  },
  validation: {
    errors: {
      username: null,
      email: null,
      roles: null
    },
    serverError: null
  },
  hasValidationErrors: Ember.computed('validation.errors', 'validation.serverError',
    function () {
      const errors = this.get('validation.errors');
      const serverError = this.get('validation.serverError');
      const e = [].concat(Object.keys(errors).map(k => errors[k]), serverError);

      return !!(e.filter(function (item) {
        return !!item;
      }).length);
    }
  ),
  validate ({username, email, roles}) {
    let validated = true;
    let errors = {};

    if (!username) {
      errors['username'] = 'Username cannot be empty';

    }
    if (!email) {
      errors['email'] = 'Email cannot be empty';
    }
    if (!roles || !roles.length) {
      errors['roles'] = 'Roles cannot be empty';
    }

    this.set('validation.errors', errors);
    validated = !Object.keys(errors).length;

    return validated;
  }
});
