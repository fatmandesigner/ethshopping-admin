import Ember from 'ember';

export default Ember.Controller.extend({
  paging: {
    size: 20,
    cursor: [undefined]
  },

  actions: {
    viewMore () {
      console.log('Loading more items...');
    },

    refresh() {
      // Reload the router
      this.get('target.router').refresh();
    }
  }
});
