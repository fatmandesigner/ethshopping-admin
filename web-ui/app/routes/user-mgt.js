import config from '../config/environment';
import Ember from 'ember';

const {APP: {apiBaseUrl: API_BASE_URL }} = config;

export default Ember.Route.extend({
  queryParams: {
    cursor: {
      refreshModel: true
    },
    q: {
      refreshModel: true
    }
  },
  model (params, transition) {
    const {size = 20} = params;
    const q = params.q || 'status:active';
    const url = `${API_BASE_URL}users--getUsers?size=${size}&q=${q}`;

    return Ember.$.get(url).promise().then(data => {
      const {users, meta} = data;
      return {users, size, meta};
    });
  },

  afterModel ({size, meta}) {
    const controller = this.controllerFor('user-mgt');
    if (meta.hasMore) {
      // const nextCursor = meta.endCursor;
      // controller.set('paging.nextCursor', nextCursor);
      controller.set('paging.cursor', meta.endCursor);
    }

    controller.set('paging.size', size);
  }
});
