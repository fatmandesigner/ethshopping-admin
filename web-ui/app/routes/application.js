import Ember from 'ember';

export default Ember.Route.extend({
  activeModal: null,
  actions: {
    showModal(modalName) {
      const $activeModal = Ember.$(`.ui.modal.${modalName}`);
      if (!$activeModal.length) {
        return;
      }

      this.set('activeModal', $activeModal);
      $activeModal.modal('show');
    }
  }
});
