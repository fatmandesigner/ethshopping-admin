'use strict';
const crypto = require('crypto');
const passwordGenerator = require('password-generator');
const MD5_SALT = '3th3r3um';

const Datastore = require('@google-cloud/datastore');
const datastore = Datastore();

const DEFAULT_NUM_RESULTS_PER_PAGE = 20;

exports.getUsers = (req, res) => {
  res = cors(res);
  
  const size = parseInt(req.query.size || DEFAULT_NUM_RESULTS_PER_PAGE, 10);
  const q = req.query.q || 'status:active';
  const filters = q.split(',').map(part => part.split(':'));
  console.log('filters:', filters);
  
  const cursor = req.query.cursor;
  var query = datastore.createQuery('User');//.select(['username', 'roles', 'email']);
  query = ['username', 'roles', 'email'].reduce((query, item) => query.order(item), query);
  
  if (cursor) {
    query = query.start(cursor);
  } else {
    query = filters.reduce((query, [k, v]) => query.filter(k, v), query);
  }
  query = query.limit(size);

  query.run().then((data) => {
    const [users, meta] = data;
    
    if (data[1].moreResults === 'MORE_RESULTS_AFTER_LIMIT') {
      res.status(200).json({ 
        users: users.map(item => (delete item['password'], item)),
        meta: {
          hasMore: true,
          endCursor: meta.endCursor
        }
      });
      return;
    }
    else {
      res.status(200).json({ 
        users: users.map(item => (delete item['password'], item)),
        meta: {
          hasMore: false
        }
      });
      return;
    }
  }).catch(errorHandler(res));
};

exports.createUser = (req, res) => {
  res = cors(res);
  
  const data = req.body;
  const key = datastore.key(['User', data.username]);

  // TODO use async.auto??
  datastore.get(key).then(function exists([_]) {
    if (_) {
      throw {status: 400, message: 'User exists'};
    }
    
    const {raw, hashed} = generatePassword(MD5_SALT);
    const entity = { key, data: Object.assign({}, data, { password: hashed }) };
    console.log(`User ${data.username} will be created. Password: ${raw}`);

    return datastore.save(entity);
  }).then(function save (entity) {
    res.status(200).json(entity).end();
  }).catch(errorHandler(res));
};

exports.applyRoles = (req, res) => {
  res.status(200).end();
};

function generatePassword(salt) {
  const md5 = crypto.createHash('md5');
  
  const raw = passwordGenerator();
  const hashed = md5.update(raw + '$' + salt).digest().toString('hex');
  
  return {
    raw, hashed
  };
}

function errorHandler(res) {
  return (e) => {
    const { status, message } = e;
    
    if (status && message) {
      res.status(status).send(message).end();
    }
    else {
      res.status(500).send(e.message).end();
    }
  };
}

function cors(res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  
  return res;
}