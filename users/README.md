API Doc
====

## Definitions

- user: A non-public user managed by ETHShopping Platform, in the payroll of ETHShopping Platform
- permission: A namespaced token with which user may or may not perform certain actions
- role: A group of permissions

More on the actual [RBAC structure](#)

## Public Methods

### Create user

### List user

### Find user by ID

### Assign roles to user