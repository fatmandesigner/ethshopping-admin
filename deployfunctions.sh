#!/bin/bash
SOURCE_URL=https://source.developers.google.com/p/ethshopping-171305/r/ethshopping-admin
alias gfd='gcloud beta functions deploy'

while read p; do
  params=(${p//;/ })
  furi="${params[0]}"
  fmod="${params[1]}"
  fentry="${params[2]}"
  
  echo "Deploying $furi from module $fmod.$fentry..."
  gcloud beta functions deploy $furi --entry-point $fentry --source-url $SOURCE_URL --source-path $fmod --trigger-http
done <FuncFile
