#!/bin/bash
echo "Using credential file `pwd`/serverless-framework-key.json"

GCLOUD_PROJECT=ethshopping-171305 GOOGLE_APPLICATION_CREDENTIALS=`pwd`/serverless-framework-key.json functions start

echo "Dev functions active..."
