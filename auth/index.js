'use strict';
const crypto = require('crypto');
const commons = require('./commons');
const Datastore = require('@google-cloud/datastore');
const jwt = require('jsonwebtoken');

const datastore = Datastore();
const MD5_SALT = '3th3r3um';
const JWT_SECRET = 's3cr3t';
/**
 *
 */
exports.authenticate = (req, res) => {
  res = commons.cors(res);
  
  if (req.method !== 'POST') {
    return res.status(405).end();
  }
  
  const data = req.body;
  const {missingFields} = validate(data);
  if (missingFields.length) {
    return res.status(400).send('Invalid login data');
  }
  
  if (data.source !== 'public' && data.source !== 'admin') {
    return res.status(400).send('Invalid login data'); 
  }
  
  const {username, password} = data;
  let authPromise;  
  
  if (data.source === 'public') {
    authPromise = authenticatePublicMember(username, password);
  }
  else if (data.source === 'admin') {
    authPromise = authenticateAdminUser(username, password);
  }
  
  authPromise.then(result => {
    res.status(200).json(result);
  }).catch(commons.errorHandler(res));
};

exports.getPermissions = (request, response) => {
  response.status(200).send('Admin');
};

/**
 * @return {object} 
 */
function validate (data) {
  const VALID_FIELDS = ['username', 'password', 'source'];
  
  const missingFields = VALID_FIELDS.reduce((acc, key) => {
    if (!(key in data)) {
      acc.push(key);
    }
    
    return acc;
  }, []);
  
  return {missingFields};
}

function authenticatePublicMember (username, password) {
  console.info(`[authenticatePublicMember] Params: ${username}, ${password}`);
  
  const key = datastore.key(['Member', username]);
  
  return datastore.get(key).then(([entity]) => {
    if (!entity) {
      throw {code: 403, message: 'Invalid login'};
    }
    console.info('[authenticatePublicMember] Found', entity);
    
    const hashedPass = crypto.createHash('md5').update(password + '$' + MD5_SALT).digest().toString('hex');
    if (hashedPass !== entity.password) {
      console.info('[authenticatePublicMember] Invalid password');
      throw {code: 403, message: 'Invalid login'};
    }
    
    return createJsonToken(entity);
  });
}

function authenticateAdminUser (username, password) {
  console.info(`[authenticateAdminUser] Params: ${username}, ${password}`);
  
  const key = datastore.key(['User', username]);
  
  return datastore.get(key).then(([entity]) => {
    if (!entity) {
      throw {code: 403, message: 'Invalid login'};
    }
    console.info('[authenticateAdminUser] Found', entity);
    
    const hashedPass = crypto.createHash('md5').update(password + '$' + MD5_SALT).digest().toString('hex');
    if (hashedPass !== entity.password) {
      console.info('[authenticateAdminUser] Invalid password');
      throw {code: 403, message: 'Invalid login'};
    }
    
    return createJsonToken(entity);
  });
}

function createJsonToken (entity) {
  return jwt.sign(
    {
      sub: entity.username,
      roles: entity.roles || ['public']
    },
    JWT_SECRET,
    {
      expiresIn: '2 days'
    });
}