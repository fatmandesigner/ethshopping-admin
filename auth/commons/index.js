exports.errorHandler = function errorHandler(res) {
  return (e) => {
    const { status, message } = e;

    if (status && message) {
      res.status(status).send(message).end();
    }
    else {
      res.status(500).send(e.message).end();
    }
  };
}

exports.cors = function cors(res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  return res;
}