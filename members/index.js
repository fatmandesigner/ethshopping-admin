'use strict';
const crypto = require('crypto');
const passwordGenerator = require('password-generator');
const commons = require('./commons');
const Datastore = require('@google-cloud/datastore');

const datastore = Datastore();
const MD5_SALT = '3th3r3um';

exports.createMember = function (req, res) {
  commons.cors(res);

  const data = req.body;

  const {
    unknownFields,
    missingFields
  } = validate(data);

  if (unknownFields.length || missingFields.length) {
    return res.status(400).send('Invalid member data' + JSON.stringify(unknownFields) + JSON.stringify(missingFields));
  }
  // Check for duplicates
  const key = datastore.key(['Member', data.username]);
  datastore.get(key).then(function ([_]) {
    if (_) {
      throw {code: 400, message: 'Member exists'};
    }

    const { raw, hashed } = generatePassword(MD5_SALT);
    console.log(`Creating member ${data.username} with password ${raw}`);
    
    const entity = Object.assign(
      {
        email: '',
        status: 'active',
        wallets: [
          {
            type: 'BTC',
            address: ''
          },
          {
            type: 'ETH',
            address: ''
          } 
        ]
      }, data, {password: hashed});
    
    return datastore.save({
      key,
      data: entity
    })
  }).then(function save (entity) {
    res.status(200).json(entity).end();
  }).catch(commons.errorHandler(res));
};

function validate (data) {
  const VALID_FIELDS = ['username', 'wallet_address'];
  const REQUIRED_FIELDS = ['username', 'wallet_address'];

  const unknownFields = Object.keys(data).reduce((acc, key) => {
    if (!VALID_FIELDS.includes(key)) {
      acc.push(key);
    }

    return acc;
  }, []);

  const missingFields = REQUIRED_FIELDS.reduce((acc, key) => {
    if (!(key in data) || !data[key]) { // Undefined, null, empty string
      acc.push(key);
    }

    return acc;
  }, []);

  return {
    unknownFields,
    missingFields
  };
}


function generatePassword(salt) {
  const md5 = crypto.createHash('md5');

  const raw = passwordGenerator();
  const hashed = md5.update(raw + '$' + salt).digest().toString('hex');

  return {
    raw, hashed
  };
}