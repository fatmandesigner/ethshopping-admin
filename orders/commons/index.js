const jwt = require('jsonwebtoken');
const JWT_SECRET = 's3cr3t';

exports.errorHandler = function errorHandler(res) {
  return (e) => {
    const { status, message } = e;

    if (status && message) {
      res.status(status).send(message).end();
    }
    else {
      res.status(500).send(e.message).end();
    }
  };
}

exports.cors = function cors(res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

  return res;
}

exports.getUser = function getUser (req) {
  if (!('authorization' in req.headers) || !req.headers['authorization']) {
    return null;
  }
  
  const authorization = req.headers['authorization'];
  const [BEARER, jwtToken] = authorization.split(' ');
  
  if (BEARER !== 'bearer') {
    return null;
  }
  
  const decoded = jwt.verify(jwtToken, JWT_SECRET);
  if (!decoded) {
    return null;
  }
  
  let {sub: username, roles} = decoded;
  return {username, roles};
}