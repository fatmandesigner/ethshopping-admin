'use strict';
const crypto = require('crypto');
const commons = require('./commons');
const Datastore = require('@google-cloud/datastore');

const datastore = Datastore();
const MD5_SALT = '3th3r3um';

const ORDER_TYPE_SIMPLE_SELL_ORDER = 'SIMPLE_SELL_ORDER';
const ORDER_TYPE_SIMPLE_BUY_ORDER = 'SIMPLE_BUY_ORDER';

const ORDER_STATUS_ACTIVE = 1;
const ORDER_STATUS_COMPLETED = 2;

const DEFAULT_PAGE_SIZE = 20;

exports.getOrders = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const q = req.query.q || 'status:1';
  const filter = parseQ(q);
  
  const size = DEFAULT_PAGE_SIZE;
  
  const querySellOrders = datastore.createQuery('SellOrder').filter('status', ORDER_STATUS_ACTIVE).limit(size);
  const queryBuyOrders = datastore.createQuery('BuyOrder').filter('status', ORDER_STATUS_ACTIVE).limit(size);
  
  Promise.all([
      querySellOrders.run().then(([entities]) => entities),
      queryBuyOrders.run().then(([entities]) => entities)
    ]).then(([sellOrders, buyOrders]) => {
    const orders = [].concat(sellOrders, buyOrders).sort(({created_at: a}, {created_at: b}) => {
      return (a > b)?1:(a < b)?-1:0;
    });
    
    return orders;
  }).then(function (entities) {
    res.status(200).json({
      orders: entities.slice(0, size).map(entity => (entity.id = entity[datastore.KEY].id, entity))
    }).end();
  })
  .catch(commons.errorHandler(res));
};

exports.getSellOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const {id} = req.query;
  if (!id) {
    return res.status(400).send('Invalid request').end();
  }
  
  const key = datastore.key(['SellOrder', parseInt(id, 10)]);
  
  datastore.get(key).then(([entity]) => {
    if (!entity) {
      return res.status(404).send('Order not found').end();
    }
    
    entity.id = entity[datastore.KEY].id;
    res.status(200).json({
      sell_order: entity
    })
  }).catch(commons.errorHandler(res));
};

exports.getBuyOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const {id} = req.query;
  if (!id) {
    return res.status(400).send('Invalid request').end();
  }
  
  const key = datastore.key(['BuyOrder', parseInt(id, 10)]);
  
  datastore.get(key).then(([entity]) => {
    if (!entity) {
      return res.status(404).send('Order not found').end();
    }
    
    entity.id = entity[datastore.KEY].id;
    res.status(200).json({
      buy_order: entity
    })
  }).catch(commons.errorHandler(res));
};

exports.createSellOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const user = commons.getUser(req);
  if (!user) {
    return res.status(403).send('Unauthorized').end();
  }
  
  const data = req.body;

  const {
    unknownFields,
    missingFields
  } = validate(data);

  if (unknownFields.length || missingFields.length) {
    return res.status(400).send('Invalid order data' + JSON.stringify(unknownFields) + JSON.stringify(missingFields));
  }
  // TODO Watch for concurrency duplicates
  const key = datastore.key('SellOrder');
  const entity = Object.assign(
    {
      seller_id: user.username,
      type: ORDER_TYPE_SIMPLE_SELL_ORDER,
      status: ORDER_STATUS_ACTIVE,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    }, data);
  
  datastore.save({
    key,
    data: entity
  }).then(function save (entity) {
    res.status(200).json(entity).end();
  }).catch(commons.errorHandler(res));
};

exports.createBuyOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const user = commons.getUser(req);
  if (!user) {
    return res.status(403).send('Unauthorized').end();
  }
  
  const data = req.body;

  const {
    unknownFields,
    missingFields
  } = validate(data);

  if (unknownFields.length || missingFields.length) {
    return res.status(400).send('Invalid order data' + JSON.stringify(unknownFields) + JSON.stringify(missingFields));
  }

  // TODO Watch for concurrency duplicates
  data['username'] = user.username;
  createOrder(ORDER_TYPE_SIMPLE_BUY_ORDER, data).then(function save (entity) {
    res.status(200).json(entity).end();
  }).catch(commons.errorHandler(res));
};

exports.fulfilSellOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const user = commons.getUser(req);
  if (!user) {
    return res.status(403).send('Unauthorized').end();
  }
  
  const {
    order_id,
    wallet_id
  } = req.body;

  if (!order_id) {
    return res.status(400).send('Invalid request').end();
  }
  
  const key = datastore.key(['SellOrder', parseInt(order_id, 10)]);
  
  datastore.get(key).then(([sellOrder]) => {
    if (!sellOrder) {
      throw {
        status: 400,
        message: 'Sell order not found'
      };
    }
    
    if (user.username === sellOrder.seller_id) {
      throw {
        status: 400,
        message: 'Cannot sell to yourself'
      };
    }
    
    // TODO Transactional enforcement
    // TODO Perform ETH/BTC API calls
    let updatedSellOrder = Object.assign(
      {},
      sellOrder,
      {
        buyer_id: user.username,
        wallet_id,
        transaction_date: new Date().toISOString(),
        status: ORDER_STATUS_COMPLETED
      });
    return datastore.update({
      key,
      data: updatedSellOrder
    })
  }).then(([entity]) => {
    res.status(200).json(entity).end();
  }).catch(commons.errorHandler(res));
};

exports.fulfilBuyOrder = function (req, res) {
  res = commons.cors(res);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  
  const user = commons.getUser(req);
  if (!user) {
    return res.status(403).send('Unauthorized').end();
  }
  
  const {
    order_id,
    wallet_id
  } = req.body;
  if (!order_id) {
    return res.status(400).send('Invalid request').end();
  }
  
  const key = datastore.key(['BuyOrder', parseInt(order_id, 10)]);
  
  datastore.get(key).then(([buyOrder]) => {
    if (!buyOrder) {
      throw {
        status: 400,
        message: 'Buy order not found'
      };
    }
    
    if (user.username === buyOrder.buyer_id) {
      throw {
        status: 400,
        message: 'Cannot buy from yourself'
      };
    }
    
    // TODO Transactional enforcement
    // TODO Perform ETH/BTC API calls
    let updatedBuyOrder = Object.assign(
      {},
      buyOrder,
      {
        seller_id: user.username,
        wallet_id,
        transaction_date: new Date().toISOString(),
        status: ORDER_STATUS_COMPLETED
      });
    return datastore.update({
      key,
      data: updatedBuyOrder
    })
  }).then(([entity]) => {
    res.status(200).json(entity).end();
  }).catch(commons.errorHandler(res));
};


function validate (data) {
  const VALID_FIELDS = ['volume', 'price', 'wallet_id'];
  const REQUIRED_FIELDS = ['volume', 'price', 'wallet_id'];

  const unknownFields = Object.keys(data).reduce((acc, key) => {
    if (!VALID_FIELDS.includes(key)) {
      acc.push(key);
    }

    return acc;
  }, []);

  const missingFields = REQUIRED_FIELDS.reduce((acc, key) => {
    if (!(key in data) || !data[key]) { // Undefined, null, empty string
      acc.push(key);
    }

    return acc;
  }, []);

  return {
    unknownFields,
    missingFields
  };
}

function createOrder (orderType, data) {
  if ([ORDER_TYPE_SIMPLE_BUY_ORDER, ORDER_TYPE_SIMPLE_SELL_ORDER].indexOf(orderType) === -1) {
    throw new Error('Invalid order type');
  }
  
  let key;
  let entity;
  const {username} = data;
  delete data['username'];
  
  if (ORDER_TYPE_SIMPLE_BUY_ORDER === orderType) {
    key = datastore.key('BuyOrder');
    entity = Object.assign(
      {
        buyer_id: username,
        type: orderType,
        status: ORDER_STATUS_ACTIVE,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString()
      }, data);
  }
  else if (ORDER_TYPE_SIMPLE_SELL_ORDER === orderType) {
    key = datastore.key('SellOrder');
    entity = Object.assign(
      {
        seller_id: username,
        type: orderType,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString()
      }, data);
  }
  
  return datastore.save({
    key,
    data: entity
  });
}

function parseQ(q) {
  const filter = q.split(',').reduce((acc, [k,v]) => {
    if (!v.length) {
      return acc;
    }
    acc[k] = v;
    return acc;
  }, {});
  
  return Object.keys(filter).map((k) => [k, filter[k]]);
}