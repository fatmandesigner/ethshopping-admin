ETH Shopping Admin
====

## Architecture

### Front End Server

Front End server is an GAE instance accessible thru [https://ethshopping-171305.appspot.com]()

### No Backend

There is not an instance of BE server. Only a collection of microservice functions, namely:

- auth
- users

## Development

## Deployment

### Deploy GAE Front End Server

1. Make sure you are in $PRODUCT_DIR/appengine
1. Deploy the `default` instance front end server with `npm run deploy`

### Deploy No-Backend Microservices

1. cd into each of the microservices you would like to deploy
1. Run terminal command `serverless deploy` to deploy the service